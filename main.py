# Project No.: 3
# Author: Drew Abbo & Kaelis White
# Description: Boggle board game using recursion

from boggle_board import BoggleBoard
import random

# the board can be any size >= 1 (determines number of rows & columns)
BOARD_SIZE = 4

def main():

    # get the seed from the user (input validation)
    while True:
        seed_str = input("Enter seed: ")
        
        # if input is blank just generate a random seed
        if (len(seed_str) == 0):
            seed = random.randint(0, 1_000_000_000)
            print(f"Using random seed: {seed}")
            break

        try:
            seed = int(seed_str)
        except ValueError:
            print("Invalid input, seed must be a number")
        else:
            break

    # seed random number generator
    random.seed(seed)

    board = BoggleBoard(BOARD_SIZE)  # create the board
    board.display()  # display the board

    # get a word from the user (input validation)
    while True:
        word = input("Enter word (in UPPERcase): ").strip().upper()
        if len(word) != 0:
            break
        print("Word must contain at least 1 character")


    # see if the word is on the board
    result = board.word_on_board(word)

    # tell user if the word is on the board
    if result is not None:
        print("Nice Job!")
    else:
        print("I don't see that word.")


    # tell user if word is a palindrome
    if board.word_is_palindrome(word):
        print(f"The word {word} is a palindrome!")
    else:
        print(f"The word {word} is not a palindrome.")


    # show user the word on the board (if it's on there)
    if result is not None:
        board.display(result)
    else:
        print("Are we looking at the same board?")



if __name__ == "__main__":
    main()
