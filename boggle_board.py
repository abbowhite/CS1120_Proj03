import string
import random

class BoggleBoard():
    # size (positive int): number of rows & columns
    def __init__(self, size):
        self.__board_size = size  # size of board (number of rows & cols)
        self.__board = self.__generate_board()  # stores the 2d list of letters

        # used to find words on the board
        self.__visited_cells = self.__get_blank_bool_board()
        self.__last_cell = None

    # getter methods
    def get_board_size(self):       return self.__board_size
    def get_board(self):            return self.__board
    def get_visited_cells(self):    return self.__visited_cells
    def get_last_cell(self):        return self.__last_cell
    # setter methods
    def visit_cell(self, row, col):     self.__visited_cells[row][col] = True
    def unvisit_cell(self, row, col):   self.__visited_cells[row][col] = False
    def set_last_cell(self, row, col):  self.__last_cell = (row, col)


    # creates a 2d list of random uppercase characters based on board size
    def __generate_board(self):
        board = []
        for i in range(self.get_board_size()):
            # append a row
            board.append([])

            # add characters into the row
            for _ in range(self.get_board_size()):
                board[i].append(random.choice(string.ascii_uppercase))
        return board


    # returns a 2d list based on board size of `False` values.
    # Example: If the size is 2: `[[False, False], [False, False]]` is returned
    def __get_blank_bool_board(self):
        size = self.get_board_size()
        board = []
        for _ in range(self.get_board_size()):
            board.append([False] * size)
        return board


    # dynamically print board based on its size
    # highlight_cells: 2d list of bool values that indicate what spaces to
    #   highlight or `None`. You can pass the output of `word_is_palindrome()`
    #   for this value to highlight the word
    def display(self, highlight_cells = None):
        if highlight_cells == None:
            highlight_cells = self.__get_blank_bool_board()

        row_separator = "+---" * self.get_board_size() + '+'

        print(row_separator)
        for row_i, row in enumerate(self.get_board()):

            print("|", end = '')
            for col_i, letter in enumerate(row):
                # highlights the cell if it's in the highlight_cells list
                if (highlight_cells[row_i][col_i]):
                    print(f"<{letter}>|", end = '')
                else:
                    print(f" {letter} |", end = '')
            print('\n' + row_separator)


    # returns bool for if a word is palindrome (like "RACECAR" or "BOB")
    # NOTE: IDK why the directions specify this to be a method of this class and
    #   not just it's own function in another file
    def word_is_palindrome(self, word):
        # base case: reached end of word
        if len(word) <= 1:
            return True

        # base case: if 1st and last chars don't match
        if word[0] != word[-1]:
            return False

        # cut off 1st and last character and recursively evaluate again
        return self.word_is_palindrome(word[1:-1])


    # pass this function the word and ...
    # if the word is on the board it will return a 2d list bool values that
    #   indicate what spaces the word takes up
    # if the word is NOT on the board it will return None
    def word_on_board(self, word):
        last_cell = self.get_last_cell()

        # base case: string is empty (we found the whole string)
        if len(word) == 0:
            # add this last cell to visited cells
            self.visit_cell(last_cell[0], last_cell[1])
            return self.get_visited_cells()

        # if this is the 1st iteration
        if last_cell is None:
            # go over every cell and check for word
            for i, row in enumerate(self.get_board()):
                for j, cell in enumerate(row):
                    # go through all cells where 1st character matches
                    if cell == word[0]:
                        self.set_last_cell(i, j)
                        result = self.word_on_board(word[1:])

                        if result is not None:
                            return result
            return None

        # if it is not the 1st iteration:

        # mark this cell as visited
        self.visit_cell(last_cell[0], last_cell[1])

        adjacent_cells = [
            (last_cell[0] + 1, last_cell[1]),  # down
            (last_cell[0] - 1, last_cell[1]),  # up
            (last_cell[0], last_cell[1] + 1),  # right
            (last_cell[0], last_cell[1] - 1)   # left
        ]
        # make a list of possible moves
        possible_moves = []
        for cell in adjacent_cells:

            # make sure cell is on the board
            if not (cell[0] in range(0, self.get_board_size()) and
                    cell[1] in range(0, self.get_board_size())):
                continue

            # make sure cell hasn't been visited
            if self.get_visited_cells()[cell[0]][cell[1]]:
                continue

            # otherwise add the cell to the list of possible moves
            possible_moves.append(cell)

        # attempt all possible moves
        for move in possible_moves:
            # go through all cells where 1st character matches
            if self.get_board()[move[0]][move[1]] == word[0]:
                self.set_last_cell(move[0], move[1])
                result = self.word_on_board(word[1:])
                if result is not None:
                    return result

        # undo state changes and return None
        self.unvisit_cell(last_cell[0], last_cell[1])
        self.set_last_cell(last_cell[0], last_cell[1])
        return None
